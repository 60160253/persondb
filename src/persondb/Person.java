/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persondb;

import java.io.Serializable;

//POJO
public class Person implements Serializable {

    private String username;
    private String name;
    private String surname;
    private String password;
    private int age;

    public Person(String username, String name, String surname, String password, int age) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.age = age;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" + "username=" + username + ", name=" + name + ", surname=" + surname + ", password=" + password + ", age=" + age + '}';
    }

    

}
